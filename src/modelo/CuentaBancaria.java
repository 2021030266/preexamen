package modelo;

/**
 *
 * @author Manuel
 */
public class CuentaBancaria {
    private int numeroCuenta;
    private Cliente cliente;
    private String fechaApertura;
    private String nombreBanco;
    private double porcentajeRendimiento;
    private double saldo;
    
    public CuentaBancaria() {
        cliente = new Cliente();
    }

    public int getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(int numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getFechaApertura() {
        return fechaApertura;
    }

    public void setFechaApertura(String fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    public String getNombreBanco() {
        return nombreBanco;
    }

    public void setNombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
    }

    public double getPorcentajeRendimiento() {
        return porcentajeRendimiento;
    }

    public void setPorcentajeRendimiento(double porcentajeRendimiento) {
        this.porcentajeRendimiento = porcentajeRendimiento;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public void depositar(double cantidad) {
        saldo += cantidad;
    }

    public boolean retirar(double cantidad) {
        if (cantidad <= saldo) {
            saldo -= cantidad;
            return true;
        } else {
            return false;
        }
    }

    public double calcularRendimientos() {
        return (porcentajeRendimiento * saldo) / 365;
    }
}
