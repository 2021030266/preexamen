package Controlador;

import modelo.CuentaBancaria;
import vista.dlgvista;

import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 *
 * @author Manuel
 */
public class Controlador implements ActionListener {
    private dlgvista vista;
    private CuentaBancaria cuentaBancaria;

    public Controlador(dlgvista vista, CuentaBancaria cuentaBancaria) {
        this.vista = vista;
        this.cuentaBancaria = cuentaBancaria;

        // Configurar listeners de eventos
        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnHacerDeposito.addActionListener(this);
        vista.btnHacerRetiro.addActionListener(this);
    }

    private void iniciarVista() {
        vista.setTitle("::    BANCO   ::");
        vista.setSize(530, 472);
        vista.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.btnNuevo) {
            vista.btnGuardar.setEnabled(true);
            vista.btnHacerDeposito.setEnabled(true);
            vista.btnHacerRetiro.setEnabled(true);
            vista.txtNumCuenta.enable(true);
            vista.txtNombreC.enable(true);
            vista.txtDomiciloC.enable(true);
            vista.txtFechaNC.enable(true);
            vista.txtSexoMasculino.setEnabled(true);
            vista.txtSexoFemenino.setEnabled(true);
            vista.txtNomBanco.enable(true);
            vista.txtfechaApertura.enable(true);
            vista.txtPorcentajeR.enable(true);
            vista.txtSaldo.enable(true);
            
            limpiarCampos();
        } else if (e.getSource() == vista.btnGuardar) {
            String numeroCuentaStr = vista.txtNumCuenta.getText();
            String nombre = vista.txtNombreC.getText();
            String domicilio = vista.txtDomiciloC.getText();
            String fechaNacimiento = vista.txtFechaNC.getText();
            String sexo = vista.txtSexoMasculino.isSelected() ? "Masculino" : "Femenino";
            String nombreBanco = vista.txtNomBanco.getText();
            String fechaApertura = vista.txtfechaApertura.getText();
            String porcentajeRendimientoStr = vista.txtPorcentajeR.getText();
            String saldoStr = vista.txtSaldo.getText();

            if (numeroCuentaStr.isEmpty() || nombre.isEmpty() || domicilio.isEmpty() || fechaNacimiento.isEmpty() ||
                    nombreBanco.isEmpty() || fechaApertura.isEmpty() || porcentajeRendimientoStr.isEmpty() || saldoStr.isEmpty()) {
                JOptionPane.showMessageDialog(vista, "Por favor, complete todos los campos");
            } else {
                try {
                    int numeroCuenta = Integer.parseInt(numeroCuentaStr);
                    double porcentajeRendimiento = Double.parseDouble(porcentajeRendimientoStr);
                    double saldo = Double.parseDouble(saldoStr);

                    cuentaBancaria.setNumeroCuenta(numeroCuenta);

                    cuentaBancaria.getCliente().setNombre(nombre);
                    cuentaBancaria.getCliente().setDomicilio(domicilio);
                    cuentaBancaria.getCliente().setFechaNacimiento(fechaNacimiento);
                    cuentaBancaria.getCliente().setSexo(sexo);

                    cuentaBancaria.setNombreBanco(nombreBanco);
                    cuentaBancaria.setFechaApertura(fechaApertura);
                    cuentaBancaria.setPorcentajeRendimiento(porcentajeRendimiento);
                    cuentaBancaria.setSaldo(saldo);

                    JOptionPane.showMessageDialog(vista, "Se guardaron los datos correctamente");
                    limpiarCampos();
                    desactivarCampos();
                    vista.btnGuardar.setEnabled(false);
                    vista.btnMostrar.setEnabled(true);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(vista, "Ocurrió un error de formato: " + ex.getMessage());
                }
            }
        } 
        else if (e.getSource() == vista.btnMostrar) {
            vista.txtCantidad.enable(true);
            
            int numeroCuenta = cuentaBancaria.getNumeroCuenta();
            String nombre = cuentaBancaria.getCliente().getNombre();
            String domicilio = cuentaBancaria.getCliente().getDomicilio();
            String fechaNacimiento = cuentaBancaria.getCliente().getFechaNacimiento();
            String sexo = cuentaBancaria.getCliente().getSexo();
            String nombreBanco = cuentaBancaria.getNombreBanco();
            String fechaApertura = cuentaBancaria.getFechaApertura();
            double porcentajeRendimiento = cuentaBancaria.getPorcentajeRendimiento();
            double saldo = cuentaBancaria.getSaldo();

            vista.txtNumCuenta.setText(String.valueOf(numeroCuenta));
            vista.txtNombreC.setText(nombre);
            vista.txtDomiciloC.setText(domicilio);
            vista.txtFechaNC.setText(fechaNacimiento);

            if (sexo.equals("Masculino")) {
                vista.txtSexoMasculino.setSelected(true);
                vista.txtSexoFemenino.setSelected(false);
            } else {
                vista.txtSexoMasculino.setSelected(false);
                vista.txtSexoFemenino.setSelected(true);
            }

            vista.txtNomBanco.setText(nombreBanco);
            vista.txtfechaApertura.setText(fechaApertura);
            vista.txtPorcentajeR.setText(String.valueOf(porcentajeRendimiento));
            vista.txtSaldo.setText(String.valueOf(saldo));
        }
        else if (e.getSource() == vista.btnHacerDeposito) {      
            double cantidad = Double.parseDouble(vista.txtCantidad.getText());
            cuentaBancaria.depositar(cantidad);
            vista.txtNuevoSaldo.setText(String.valueOf(cuentaBancaria.getSaldo()));
        }
        else if (e.getSource() == vista.btnHacerRetiro)     {
            double cantidad;
            try {
                cantidad = Double.parseDouble(vista.txtCantidad.getText());
            } catch (NumberFormatException ex2) {
                JOptionPane.showMessageDialog(null, "Ingrese una cantidad válida", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }

            boolean exito = cuentaBancaria.retirar(cantidad);

            if (exito) {
                vista.txtNuevoSaldo.setText(String.valueOf(cuentaBancaria.getSaldo()));
            } else {
                int opcion = JOptionPane.showConfirmDialog(null, "No tienes fondos suficientes. ¿Deseas volver a intentarlo?", "Error", JOptionPane.YES_NO_OPTION);
                if (opcion == JOptionPane.YES_OPTION) {
                    // Volver a intentar
                    vista.txtCantidad.setText("");
                    vista.txtCantidad.requestFocus();
                } else {
                    vista.btnHacerRetiro.setEnabled(false);
                }
            }
        }
    }

    private void desactivarCampos() {
        vista.txtNumCuenta.enable(false);
        vista.txtNombreC.enable(false);
        vista.txtDomiciloC.enable(false);
        vista.txtFechaNC.enable(false);
        vista.txtSexoMasculino.enable(false);
        vista.txtSexoFemenino.enable(false);
        vista.txtNomBanco.enable(false);
        vista.txtfechaApertura.enable(false);
        vista.txtPorcentajeR.enable(false);
        vista.txtSaldo.enable(false);
    }

    private void limpiarCampos() {
        vista.txtNumCuenta.setText("");
        vista.txtNombreC.setText("");
        vista.txtDomiciloC.setText("");
        vista.txtFechaNC.setText("");
        vista.txtNomBanco.setText("");
        vista.txtfechaApertura.setText("");
        vista.txtPorcentajeR.setText("");
        vista.txtSaldo.setText("");
        vista.txtCantidad.setText("");
        vista.txtNuevoSaldo.setText("");
    }

    public static void main(String[] args) {
        dlgvista vista = new dlgvista();
        CuentaBancaria cuentaBancaria = new CuentaBancaria();
        Controlador controlador = new Controlador(vista, cuentaBancaria);
        controlador.iniciarVista();
    }
}

